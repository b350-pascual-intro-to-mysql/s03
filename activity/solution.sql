-- 1. Create a s03 and activity folder and then create a file named solution.sql and write the SQL code necessary to insert the following records in the tables and to perform the following query.
-- 2. Add the following records to the blog_db database:
--     - Check User Image
--     - Check Posts Image
-- 3. Get all the post with an Author ID of 1.
-- 4. Get all the user's email and datetime of creation.
-- 5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
-- 6. Delete the user with an email of "johndoe@gmail.com".
-- 7. Push your solution in your gitlab repository and linked it to your Boodle.
x

-- MY SOLUTIONS:

-- 2. Add the following records to the blog_db database:
--     USERS TABLE SOLUTION
INSERT INTO users (email, password, datetime_created) VALUES
("johnsmith@gmail.com", "passwordA", 20210101010000),
("juandelacruz@gmail.com", "passwordB", 20210101020000),
("janesmith@gmail.com", "passwordC", 20210101030000),
("mariadelacruz@gmail.com", "passwordD", 20210101040000),
("johndoe@gmail.com", "passwordE", 20210101050000);

--     POSTS TABLE SOLUTION
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES
(1, "First Code", "Hello World!", 20210101010000),
(2, "Second Code", "Hello Earth!", 20210101020000),
(3, "Third Code", "Welcome to Mars!", 20210101030000),
(4, "Fourth Code", "Bye bye solar system!", 20210101040000);

-- 3. Get all the post with an Author ID of 1.
SELECT title FROM posts WHERE author_id = 1;

-- 4. Get all the user's email and datetime of creation.
SELECT email, datetime_created FROM users;

-- 5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE author_id = 2;

-- 6. Delete the user with an email of "johndoe@gmail.com".
DELETE FROM users WHERE email = "johndoe@gmail.com" AND id = 5;




-- STRETCH GOAL
-- 1. Insert the following records in the music_db database.
--     USER:
--         username: johndoe
--         password: john1234
--         full name: John Doe
--         contact number: 09123456789
--         email: john@mail.com
--         address: Quezon City

INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES
("johndoe", "john1234", "John Doe", "09123456789", "john@mail.com", "Quezon City");

--     PLAYLIST:
--         johndoe's playlist
--         created at: September 20, 2023 8:00 AM

INSERT INTO playlists (user_id, datetime_created) VALUES (1, "20230920080000");

--     PLAYLIST SONG:
--         Gangnam  Style
--         Kundiman

INSERT INTO playlists_songs (playlist_id, song_id) VALUES (3, 1), (3, 2);
--     Note: Make sure to display each table to check if the record is added.

-- 2. Write the SQL code necessary to insert the following records in the solution.sql.




